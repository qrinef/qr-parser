import rpn from 'request-promise-native';
import json from '../body.json';

interface ISportEvent {
    id: string,
    sport: string;
    champ: string;
    title: string;
    team: string[];
}

class Parser {
    static async getToken() {
        const body = await rpn('https://gg.bet/ru/betting');

        return body.match(/token: \"(.*)\"/)[1];
    }

    static async getEvents() {
        const token = await this.getToken();

        const body = await rpn({
            url: "https://betting-public-graphql.gin.bet/graphql",
            json: true,
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "x-auth-token": token
            },
            body: json
        });


        let result: ISportEvent[] = [];

        for (let match of body[0].data.matches) {
            result.push({
                id: match.id,
                sport: match.fixture.sportId,
                champ: match.fixture.tournament.name,
                title: match.fixture.title,
                team: match.markets[0].odds
            });
        }

        console.log(result);
        console.log(result.length);
    };
}

Parser.getEvents();
